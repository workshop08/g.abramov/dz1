import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList RWarriors = new ArrayList();

        Warrior r_w1 = new Warrior("Russia", 5, 3, 2);
        Warrior r_w2 = new Warrior("Russia", 6, 2, 2);
        Archer p_a = new Archer("Polynesia", 4, 4, 2);
        Scout n_s = new Scout("Netherlands", 4, 4, 2);
        Horseman h_h = new Horseman("Huns", 3, 5, 2);

        Weapon w1 = new Weapon("Spear", 100, 3);
        Weapon w2 = new Weapon("Bow", 100, 3);

        Armor a1 = new Armor("Helmet", 100, 4);

        RWarriors.add(r_w1);
        RWarriors.add(r_w2);

        r_w1.equipWeapon(w1);
        r_w2.equipWeapon(w2);
        p_a.equipArmor(a1);
        p_a.equipWeapon(w2);
        p_a.equipWeapon(w1);

        w1.wnPrint();
        w2.wnPrint();
        a1.arPrint();

        r_w1.WPrint();
        r_w2.WPrint();
        p_a.APrint();
        h_h.HPrint();
    }

}