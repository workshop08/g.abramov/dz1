public class Horseman {
    private int armor, damage, moves, id;
    private String civ;
    private static int i = 0;

    public Horseman (String civ, int armor, int damage, int moves) {
        this.armor = armor;
        this.damage = damage;
        this.moves = moves;
        this.civ = civ;
        this.id = i;
        i++;
    }

    public void HPrint() {
        System.out.println("This unit is " + this.civ + "'s Horseman.\nArmor - " + this.armor
                + ".\nDamage - " + this.damage + ".\nMoves - " + this.moves + ".\nID - " + this.id + ".\n");
    }

    public int getId() {
        return this.id;
    }

    public int getDamage() {
        return this.damage;
    }

    public void setDamage(int a) {
        this.damage = a;
    }

    public int getMoves() {
        return this.moves;
    }

    public void setMoves(int a) {
        this.moves = a;
    }

    public String getCiv() {
        return this.civ;
    }

    public void setCiv(String a) {
        this.civ = a;
    }

    public int getArmor() {
        return this.armor;
    }

    public void setArmor(int a) {
        this.armor = a;
    }

}