public class Armor {
    private int defense, status, id;
    private String type;
    private static int i = 0;

    public Armor(String type, int status, int defense) {
        this.defense = defense;
        this.type = type;
        this.status = status;
        this.id = i;
        i++;
    }

    public void arPrint() {
        System.out.println("This is " + this.type + ".\nDefense - " + this.defense + ".\nStatus - " + this.status
                + ".\nID - " + this.id + ".\n");
    }

    public int getId() {
        return this.id;
    }

    public int getDefense() {
        return this.defense;
    }

    public void setDefense(int a) {
        this.defense = a;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int a) {
        this.status = a;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String a) {
        this.type = a;
    }
}