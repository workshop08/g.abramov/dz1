public class Weapon {
    private int damage, status, id;
    private String type;
    private static int i = 0;

    public Weapon(String type, int status, int damage) {
        this.damage = damage;
        this.type = type;
        this.status = status;
        this.id = i;
        i++;
    }

    public void wnPrint() {
        System.out.println("This is " + this.type + ".\nDamage - " + this.damage + ".\nStatus - " + this.status
                + ".\nID - " + this.id + ".\n");
    }

    public int getId() {
        return this.id;
    }

    public int getDamage() {
        return this.damage;
    }

    public void setDamage(int a) {
        this.damage = a;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int a) {
        this.status = a;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String a) {
        this.type = a;
    }
}