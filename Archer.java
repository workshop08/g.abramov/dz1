public class Archer {
    private int armor, damage, moves, id;
    private String civ;
    private static int i = 0;

    public Archer(String civ, int armor, int damage, int moves) {
        this.armor = armor;
        this.damage = damage;
        this.moves = moves;
        this.civ = civ;
        this.id = i;
        i++;
    }

    public void APrint() {
        System.out.println("This unit is " + this.civ + "'s Archer.\nArmor - " + this.armor
                + ".\nDamage - " + this.damage + ".\nMoves - " + this.moves + ".\nID - " + this.id + ".\n");
    }

    public void equipWeapon(Weapon item) {
        if (item.getType() == "Bow") {
            this.damage += item.getDamage();
        }
        else {
            System.out.println("This unit can't equip this item");
        }
    }

    public void equipArmor(Armor item) {
        if (item.getType() == "Helmet") {
            this.armor += item.getDefense();
        }
        else {
            System.out.println("This unit can't equip this item");
        }
    }

    public int getId() {
        return this.id;
    }

    public int getDamage() {
        return this.damage;
    }

    public void setDamage(int a) {
        this.damage = a;
    }

    public int getMoves() {
        return this.moves;
    }

    public void setMoves(int a) {
        this.moves = a;
    }

    public String getCiv() {
        return this.civ;
    }

    public void setCiv(String a) {
        this.civ = a;
    }

    public int getArmor() {
        return this.armor;
    }

    public void setArmor(int a) {
        this.armor = a;
    }

}